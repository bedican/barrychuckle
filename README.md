# BarryChuckle

BarryChuckle is the client component of the [ChuckleBrothers](https://www.npmjs.com/package/chucklebrothers)

## Installation

``` bash
  $ npm install barrychuckle
```

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
