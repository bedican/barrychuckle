var http = require('http');
var request = require('request');

var barry = {
    chuckle: function(endpoint, callback) {

        var _this = this;

        var options = {
            url: endpoint,
            json: true,
            body: { 'paul': 'To me' },
            headers: {
                'Content-Type': 'application/json'
            }
        };

        request.post(options, function(error, response, body) {
            if ((error) || (response.statusCode != 200)) {
                callback(false, { error: error.code });
                return;
            }

            callback((body.barry && body.barry == 'To you'), body);
        });
    }
};

module.exports = {
    chuckle: function(endpoint, callback) {
        barry.chuckle(endpoint, callback);
    }
};
